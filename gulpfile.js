var gulp		 = require('gulp'), 							// Подключаем Gulp
	sass		 = require('gulp-sass'), 						//Подключаем Sass пакет,
	browserSync  = require('browser-sync'), 					// Подключаем Browser Sync
	concat		 = require('gulp-concat'), 						// Подключаем gulp-concat (для конкатенации файлов)
	uglify		 = require('gulp-uglifyjs'), 					// Подключаем gulp-uglifyjs (для сжатия JS)
	cssnano		 = require('gulp-cssnano'), 					// Подключаем пакет для минификации CSS
	rename		 = require('gulp-rename'), 						// Подключаем библиотеку для переименования файлов
	del			 = require('del'), 								// Подключаем библиотеку для удаления файлов и папок
	imagemin	 = require('gulp-imagemin'), 					// Подключаем библиотеку для работы с изображениями
	pngquant	 = require('imagemin-pngquant'), 				// Подключаем библиотеку для работы с png
	cache		 = require('gulp-cache'), 						// Подключаем библиотеку кеширования
	autoprefixer = require('gulp-autoprefixer'),				// Подключаем библиотеку для автоматического добавления префиксов
	tsc 		 = require('gulp-typescript-compiler'),			// компиляция TS -> JS
	plumber		 = require('gulp-plumber');						//ловим ошибки



var prod			= 'assets',					   	//боевой	Проект
	prod_dir_img	= prod+'/img',			  		//JS		Folder
	prod_dir_css	= prod+'/css',			  		//JS		Folder
	prod_dir_js	 	= prod+'/js',			  		//JS		Folder
	prod_dir_fonts  = prod+'/fonts',				//Fonts	    Folder

	dev				= 'dev',						//DEV	    Проект
	dev_dir_img 	= dev+'/img',					//JS		Folder
	dev_dir_css		= dev+'/css',					//CSS	    Folder
	dev_dir_scss	= dev+'/scss',					//SCSS	    Folder
	dev_dir_js		= dev+'/js',					//JS		Folder
	dev_dir_ts	 	= dev+'/ts',					//TS		Folder
	dev_dir_fonts   = dev+'/fonts',					//Fonts	    Folder
	dev_dir_libs    = dev+'/libs',					//Fonts	    Folder

	html_file   	= '/**/*.html',		 			//HTML  File
	php_file		= '/**/*.php',		  			//PHP   File

	img_file		= dev_dir_img+'/**/*',			//IMG   File
	css_file		= dev_dir_css+'/**/*.css',		//CSS   File
	js_file	 		= dev_dir_js+'/**/*.js',		//JS	File
	scss_file   	= dev_dir_scss+'/**/*.scss',	//SCSS  File
	ts_file	 		= dev_dir_ts+'/**/*.ts';		//TS	File
	fonts_file	 	= dev_dir_fonts+'/**/*';		//Fonts	File
	css_libs	 	= [								//CSS	Libs
		dev_dir_libs+'/magnific-popup/dist/magnific-popup.css'
	];
	js_libs	 		= [								//JS	Libs
		dev_dir_libs+'/jquery/dist/jquery.js',
		dev_dir_libs+'/magnific-popup/dist/jquery.magnific-popup.js'
	];

	domain	  		= "happyfamealy.mydev.name";	//domain


gulp.task('sass', function(){ // Создаем таск Sass
	return gulp.src(scss_file) // Берем источник
		.pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
		.pipe(gulp.dest(prod_dir_css)) // Выгружаем результата в папку app/css
		.pipe(browserSync.reload({stream: true})); // Обновляем CSS на странице при изменении
});

gulp.task('browser-sync', function() { // Создаем таск browser-sync
	browserSync({ // Выполняем browserSync
		// server: { // Определяем параметры сервера
		// 	baseDir: prod // Директория для сервера - app
		// },
		proxy: domain,
		notify: false // Отключаем уведомления
	});
});

gulp.task('scripts', ['typescript', 'scripts-libs'], function() {
	return gulp.src(js_file)
		.pipe(concat('scripts.min.js')) // Собираем их в кучу в новом файле libs.min.js
		.pipe(uglify()) // Сжимаем JS файл
		.pipe(gulp.dest(prod_dir_js)); // Выгружаем в папку app/js
});

gulp.task('scripts-libs', function() {
	return gulp.src(js_libs)
		.pipe(concat('libs.min.js')) // Собираем их в кучу в новом файле libs.min.js
		.pipe(uglify()) // Сжимаем JS файл
		.pipe(gulp.dest(prod_dir_js)); // Выгружаем в папку app/js
});

//Скрипты TS проекта
gulp.task('typescript', function () {
	return gulp
		.src(ts_file)
		.pipe(plumber())
		.pipe(tsc({
			module: '',
			target: 'ES5',
			sourcemap: false,
			logErrors: false
		}))
		.pipe(uglify()) // Сжимаем файл
		.pipe(gulp.dest(prod_dir_js));
});

gulp.task('css-libs', function() {
	return gulp.src(css_libs) // Выбираем файл для минификации
		.pipe(cssnano()) // Сжимаем
		.pipe(rename({suffix: '.libs.min'})) // Добавляем суффикс .min
		.pipe(gulp.dest(prod_dir_css)); // Выгружаем в папку app/css
});

gulp.task('style', ['sass', 'css-libs'], function() {
	return gulp.src(css_file) // Выбираем файл для минификации
		.pipe(cssnano()) // Сжимаем
		.pipe(rename({suffix: '.css.min'})) // Добавляем суффикс .min
		.pipe(gulp.dest(prod_dir_css)); // Выгружаем в папку app/css
});

gulp.task('watch', ['browser-sync', 'style', 'scripts'], function() {
	gulp.watch(scss_file, ['sass']); // Наблюдение за sass файлами в папке sass
	gulp.watch(css_file, ['style']); // Наблюдение за sass файлами в папке sass
	gulp.watch(html_file, browserSync.reload); // Наблюдение за HTML файлами в корне проекта
	gulp.watch(php_file, browserSync.reload); // Наблюдение за HTML файлами в корне проекта
	gulp.watch(ts_file, browserSync.reload);   // Наблюдение за TS файлами в папке ts
	gulp.watch(js_file, browserSync.reload);   // Наблюдение за JS файлами в папке js
});

gulp.task('clean', function() {
	return del.sync(prod); // Удаляем папку dist перед сборкой
});

gulp.task('img', function() {
	return gulp.src(img_file) // Берем все изображения из app
		.pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest(prod_dir_img)); // Выгружаем на продакшен
});

gulp.task('build', ['clean', 'img', 'style', 'scripts'], function() {

	var buildFonts = gulp.src(fonts_file) // Переносим шрифты в продакшен
		.pipe(gulp.dest(prod_dir_fonts));

});

gulp.task('clear', function (callback) {
	return cache.clearAll();
});

gulp.task('default', ['watch']);

// //отправить файлы на сервер
// gulp.task('deploy', function() {
// 	var conn = ftp.create({
// 		host:      'hostname.com',
// 		user:      'username',
// 		password:  'userpassword',
// 		parallel:  10,
// 		log: gutil.log
// 	});
// 	var globs = [
// 		prod+'/**',
// 		html_file,
// 		php_file
// 	];
// 	return gulp.src(globs, {buffer: false})
// 		.pipe(conn.dest('/path/to/folder/on/server'));
// });